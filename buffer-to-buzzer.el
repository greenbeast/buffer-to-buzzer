;;;; This Emacs package will interface with mmcli to send and receive texts as
;;;; well as store them in a local database.  It will also provide a
;;;; convenient interface to the contacts database.
;;;;
;;;; This package is licensed under the MIT license.
;;;;

;;; Code:
;; MESSAGING OPTIONS
;; All messaging options must be used with --modem or -m.

;; --messaging-status
;;     Show the status of the messaging support. 
;; --messaging-list-sms
;;     List SMS messages available on a given modem. 
;; --messaging-create-sms=['KEY1=VALUE1,...']
;;     Create a new SMS on a given modem. KEYs can be any of the following:

;;         'number'

;;              Number to which the message is addressed. 
;;         'text'
;;             Message text, in UTF-8. When sending, if the text is larger than the limit of the technology or modem, the message will be broken into multiple parts or messages. Note that text and data are never given at the same time. 
;;         'smsc'
;;             Indicates the SMS service center number. 
;;         'validity'
;;             Specifies when the SMS expires in the SMSC. 
;;         'class'
;;             3GPP message class (0..3). 
;;         'delivery-report-request'
;;             Specifies whether delivery report is requested when sending the SMS ('yes' or 'no') 
;;         'storage'
;;             Specifies the storage where this message is kept. Storages may be 'sm', 'me', 'mt', 'sr', 'bm', 'ta'. 

;; --messaging-create-sms-with-data=PATH
;;     Use PATH to a filename as the data to create a new SMS. 
;; --messaging-delete-sms=PATH
;;     Delete an SMS from a given modem. PATH indicates the SMS path. 

;; Creating a new SMS message & storing it

;; Using the â€œsmâ€ (SIM), you can do this using:

;;     $ mmcli -m 0 --messaging-create-sms="text='Hello world',number='+1234567890'"
;;     Successfully created new SMS:
;;         /org/freedesktop/ModemManager1/SMS/21 (unknown)

;;     $ sudo mmcli -s 21 --store-in-storage="sm"
;;     successfully stored the SMS

;;     $ sudo mmcli -s 21
;;     SMS '/org/freedesktop/ModemManager1/SMS/21'
;;       -----------------------------------
;;       Content    |              number: '+1234567890'
;;                  |                text: 'Hello world'
;;       -----------------------------------
;;       Properties |            PDU type: 'submit'
;;                  |               state: 'stored'
;;                  |                smsc: 'unknown'
;;                  |            validity: '0'
;;                  |               class: '0'
;;                  |             storage: 'sm'
;;                  |     delivery report: 'not requested'
;;                  |   message reference: '0'

;;     $ sudo mmcli -m 0 --messaging-status
;;     /org/freedesktop/ModemManager1/Modem/0
;;       ----------------------------
;;       Messaging | supported storages: 'sm, me'
;;                 |    default storage: 'me'


;; Sending SMS messages from files

;; As you can see below, the important part is the --messaging-create-sms-with-data and the PATH provided.

;;     $ sudo mmcli -m 0 \
;;            --messaging-create-sms="number='+1234567890'" \
;;            --messaging-create-sms-with-data=/path/to/your/file
;;     Successfully created new SMS:
;;         /org/freedesktop/ModemManager1/SMS/22 (unknown)

;;     $ sudo mmcli -s 22 --send
;;     successfully sent the SMS

 
;; Listing SMS messages

;; When the receiver gets all the parts of the message, they can now recover the sent file with another mmcli command in their ModemManager setup:

;;     $> sudo mmcli -m 0 --messaging-list-sms
;;     Found 1 SMS messages:
;;         /org/freedesktop/ModemManager1/SMS/0 (received)

;;     $> sudo mmcli -s 0 --create-file-with-data=/path/to/the/output/file

;; User
;; Creating a message

;; $ sudo mmcli -m 0 --messaging-create-sms="text='Hello world',number='+1234567890'"
;; Successfully created new SMS:
;;     /org/freedesktop/ModemManager1/SMS/12 (unknown)


;;     retrieve sms id from output (here: 12)

;; send sms:

;; $ sudo mmcli -s 12 --send
;; successfully sent the SMS

;; list modem settings:

;;     full:

;;     $ mmcli -m 0

;;     simplified output:

;;     $ mmcli -m 0 --simple-status

;; list sms in memory:

;; $ mmcli -m 0 --messaging-list-sms

;; read received sms:

;; $ mmcli -s XY

;; where XY is a number from the list sms statement
;; :TODO: Rename all functions to mmcli-*
;; Package name: buffer-to-buzzer
;; Version: 0.0.1

(require 'emacsql)

(defvar db-path "~/.emacs.d/buffer-to-buzzer.db"
  "Path to the database file.")

;; function to get the modem index
(defun get-modem-index ()
  "Get the modem index."
  (string-to-number
   (shell-command-to-string "mmcli -L | grep Modem/ | awk -F/ '{print $NF}' | tr -cd '[:digit:]'")))

(defun modem-status ()
  "Check the status of the modem."
  (shell-command-to-string (concat "mmcli -m " (number-to-string (get-modem-index)) " --messaging-status")))

(defun modem-list-sms ()
  "List SMS messages available on a given modem."
  (shell-command-to-string (concat "mmcli -m " (number-to-string (get-modem-index)) " --messaging-list-sms")))

;; (defvar db (emacsql-sqlite "~/company.db"))

;; ;; Create a table. Table and column identifiers are symbols.
;; (emacsql db [:create-table people ([name id salary])])

;; ;; Or optionally provide column constraints.
;; (emacsql db [:create-table people
;;              ([name (id integer :primary-key) (salary float)])])

;; ;; Insert some data:
;; (emacsql db [:insert :into people
;;              :values (["Jeff" 1000 60000.0] ["Susan" 1001 64000.0])])

;; ;; Query the database for results:
;; (emacsql db [:select [name id]
;;              :from people
;;              :where (> salary 62000)])
;; ;; => (("Susan" 1001))

;; ;; Queries can be templates, using $1, $2, etc.:
;; (emacsql db [:select [name id]
;;              :from people
;;              :where (> salary $s1)]
;;          50000)
;; ;; => (("Jeff" 1000) ("Susan" 1001))

;; Function to create a connection to the database
(defun connect-to-database ()
  "Connect to the database."
  (emacsql-sqlite db-path))

(defun create-db ()
  "Create a database."
  (emacsql-sqlite db-path
		  [:create-table sms
				  ([id integer :primary-key]
				   [number string]
				   [text string]
				   [state string]
				   [delivery-report-request string])]))

;;; using emacsql to insert the number, sms and timestamp into the database
(defun insert-sms (number sms timestamp delivery-report-request)
  "Insert the number, sms and timestamp into the database."
  (let ((db (emacsql-sqlite db-path)))
    (emacsql db [:insert :into sms
		 :values ([$s1 $s2 $s3 $s4])
		 :where (and (= number $s1) (= sms $s2) (= timestamp $s3) (= delivery-report-request $s4))]
	     number sms timestamp delivery-report-request)))

;; using emacsql to retrieve the sms from the database and put them in an org table based on the number and timestamp
(defun retrieve-sms ()
  "Retrieve the sms from the database and put them in an org table based on the number and timestamp."
  (let ((db (emacsql-sqlite db-path)))
    (emacsql db [:select [number text]
		 :from sms
		 :order-by (asc number) (asc timestamp)])))

;; Function to format and organize SMS data for Org tables
(defun format-sms-for-org ()
  "Format and organize SMS data for Org tables."
  (interactive)
  (let* ((sms-data (retrieve-sms))
         (phone-numbers (delete-dups (mapcar #'car sms-data))))
    (dolist (number phone-numbers)
      (let* ((filtered-data (cl-remove-if-not (lambda (sms) (equal (car sms) number)) sms-data))
             (org-table (format "| %s | %s | %s |\n" "Number" "Text" "Timestamp")))
        (dolist (sms filtered-data)
          (setq org-table (concat org-table (format "| %s | %s | %s |\n" (nth 0 sms) (nth 1 sms) (nth 2 sms)))))
        (message org-table)))))

(provide 'buffer-to-buzzer)
;;; buffer-to-buzzer.el ends here
